WattSim Backend
===============

Prérequis
---------

* Python 3.5 ou ultérieur
* Un clone Git du dépôt (``git clone https://gitlab.com/wattsim/backend``)
* Recommandé : Un outil de gestion d'environnements virtuels Python
  tel que `virtualenvwrapper`_ ou `conda`_

Installation
------------

Une fois un environnement virtuel Python configuré, on installe le serveur :

* Pour le développement : ``pip3 install -e .[dev]``
* Pour seulement faire fonctionner l'application : ``pip3 install -e .``

On prépare ensuite la base de données ::

   cd wattsim
   ./manage.py migrate

Enfin, on peut créer un compte administrateur ::

   ./manage.py createsuperuser

Il vous sera demandé de saisir un nom d'utilisateur, une adresse e-mail,
et un mot de passe. Et le tour est joué.

Exécution
---------

Pour démarrer le serveur, rien de plus simple ::

   ./manage.py runserver

Le serveur s'exécutera alors sur http://localhost:8000/.

Utilisation dans un script
--------------------------

Pour effectuer des appels à l'API depuis un script Python, avec le package
`requests`_, on utilisera une `session`_.

.. code:: python

   import requests
   session = requests.Session()

   # Authentification
   from getpass import getpass
   username = input("Nom d'utilisateur : ")
   password = getpass('Mot de passe :')
   response = session.post(
       'http://localhost:8000/api/v1/user/',
       json={'username': username, 'password': password},
   )
   response.raise_for_status()

   # Paramétrage de la session pour renvoyer correctement les informations
   # d'authentification
   session.headers['Referer'] = 'http://localhost:8000'
   session.headers['X-CSRFToken'] = session.cookies['csrftoken']

   # Envoyer une mesure de sa station
   response = session.post(
       'http://localhost:8000/api/v1/measurements/',
       json={
           'solar': 10.0,
           'wind': None,  # Pas de mesure à fournir
           'hydro': 2.0,
       }
   )
   response.raise_for_status()
   print(response.json())

Configuration de déploiement
----------------------------

Il est possible d'utiliser les variables d'environnement suivantes pour
configurer différemment le serveur en production :

``DEBUG``
   Définir cette valeur à ``1``, ``true``, ou ``yes`` pour autoriser le mode
   débogage. Définir à toute autre valeur pour désactiver le mode débogage
   et passer en mode production. **Activé par défaut.**
``SECRET_KEY``
   Clé secrète utilisée pour la génération des cookies de sessions.
   Si cette clé est disponible publiquement, toutes les sessions utilisateur
   peuvent être compromises.
``DB_ENGINE``
   Moteur de base de données à utiliser. SQLite par défaut.

   Voir https://docs.djangoproject.com/en/2.2/ref/settings/#engine
``DB_HOST``
   Nom d'hôte du serveur de base de données. Non utilisé avec SQLite.
``DB_PORT``
   Port du serveur de base de données. Non utilisé avec SQLite.
``DB_NAME``
   Nom de la base de données à utiliser.
   Sous SQLite, chemin du fichier de base de données à utiliser.
   Par défaut, utilisera ``db.sqlite3`` dans le répertoire du package.
``DB_USER``
   Nom d'utilisateur à utiliser pour la connexion au serveur de base de
   données. Non utilisé avec SQLite.
``DB_PASSWORD``
   Mot de passe à utiliser pour la connexion au serveur de base de données.
   Non utilisé avec SQLite.
``CORS_ORIGIN_WHITELIST``
   Liste d'origines autorisées dans la politique CORS. Devrait contenir
   les URLs du frontend et du backend pour autoriser le frontend à transmettre
   les informations de connexion et accéder à l'API.

   Voir https://github.com/OttoYiu/django-cors-headers#cors_origin_whitelist
``CSRF_TRUSTED_ORIGINS``
   Liste de noms d'hôtes que Django autorisera dans le header ``Referrer``
   dans le cas de requêtes POST, PUT, PATCH ou DELETE. Voir
   https://docs.djangoproject.com/en/2.2/ref/settings/#csrf-trusted-origins

   Par défaut, n'autorise que ``localhost``. Devrait contenir au moins les
   nom d'hôte du frontend et du backend pour autoriser les requêtes
   POST, PUT, PATCH, et DELETE sur l'API par le frontend et les scripts Python.

.. _virtualenvwrapper: https://virtualenvwrapper.readthedocs.io/en/latest/
.. _conda: https://conda.io/en/latest/
.. _requests: https://2.python-requests.org/en/master/
.. _session: https://2.python-requests.org/en/master/user/advanced/#session-objects
