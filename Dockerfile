FROM python:3-alpine
LABEL maintainer="Erwan Rouchet <lucidiot@protonmail.com>"
EXPOSE 80

ADD . /src
RUN cd /src && pip install . && cd .. && rm -rf src

ENTRYPOINT ["manage.py"]
CMD ["runserver", "0.0.0.0:80"]
