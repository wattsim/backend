from django.conf import settings
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('api/v1/', include('wattsim.app.urls', namespace='v1')),
    path('admin/', admin.site.urls),
]


if settings.DEBUG:
    try:
        import debug_toolbar  # noqa: F401
        urlpatterns.insert(0, path('__debug__/', include(debug_toolbar.urls)))
    except ImportError:
        pass
