from django.urls import path
from wattsim.app.views import UserAPIView, MeasurementAPIView, LatestMeasurementAPIView


app_name = 'wattsim.app'
urlpatterns = [
    path('user/', UserAPIView.as_view(), name='user'),
    path('measurements/latest/', LatestMeasurementAPIView.as_view(), name='latest-measurement'),
    path('measurements/', MeasurementAPIView.as_view(), name='measurements'),
]
